import vk, json, urllib
from flask import Blueprint, request, render_template, flash, redirect, url_for
from flask_login import current_user, login_user, logout_user, login_required
from app.model import *
from app import app, login_manager


def construct_mod_auth(starting_page):
	mod_auth = Blueprint('auth', __name__, template_folder='templates')

	@mod_auth.route('/login/')
	def login():
		if current_user.is_authenticated:
			return redirect(url_for(starting_page))
		else:
			return render_template('login.html')

	@mod_auth.route('/login/vk/')
	def login_vk():
		if 'code' in request.args:
			user_info = vk_get_user_info(request.args['code'])
			if user_info == None:
				flash('VK login failed')
				return redirect(url_for('auth.login'))

			user, created =  User.get_or_create(
					vk_id=user_info['uid'],
					defaults={
						'vk_access_token' : user_info['access_token'],
						'first_name' : user_info['first_name'],
						'last_name' : user_info['last_name']
						})

			login_user(user)
			return redirect(url_for(starting_page))

		else:
			params = {
					'client_id' : app.config['VK_APP_ID'],
					'redirect_uri' : url_for('auth.login_vk', _external=True),
					'response_type' : 'code',
					'v' : '5.60',
					'scope' : str(0xFFFFFFFF)
					}
			url = 'https://oauth.vk.com/authorize?' + urllib.parse.urlencode(params)
			return redirect(url)

	@mod_auth.route('/logout/')
	def logout():
		logout_user()
		return redirect(url_for('auth.login'))

	return mod_auth

@login_manager.user_loader
def load_user(user_id):
	try:
		return User.get(User.vk_id == user_id)
	except:
		return None

def vk_get_user_info(code):
	params = {
			'client_id' : app.config['VK_APP_ID'],
			'client_secret' : app.config['VK_APP_SECRET'],
			'redirect_uri' : url_for('auth.login_vk', _external=True),
			'code' : code 
			}
	url = 'https://oauth.vk.com/access_token?' + urllib.parse.urlencode(params)

	try:
		with urllib.request.urlopen(url) as f:
			data = f.read()
			decoded_data = data.decode('utf-8')
			user_obj = json.loads(decoded_data)
	except Exception as e:
		return None
	
	access_token = user_obj['access_token']

	vk_session = vk.Session(access_token=access_token)
	vk_api = vk.API(vk_session)

	try:
		user_data = vk_api.users.get(user_ids=0)[0]
		user_data['access_token'] = access_token
		return user_data
	except Exception as e:
		return None
