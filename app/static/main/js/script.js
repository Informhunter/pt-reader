function update_form_source_names() {
	var name_inputs = $('.sources-name');
	var url_inputs = $('.sources-url');
	for(var i = 0; i < name_inputs.length; i++) {
		name_inputs[i].name = 'sources-'+i.toString()+'-name';
		url_inputs[i].name = 'sources-'+i.toString()+'-url';
	}
}

$(document).ready(function() {
	$(".add-source").click(function(event) {
		source = '<div class="source_wrap"><table cols="3" rows="2"><tr><td>Source name:</td><td><input class="sources-name" value="" type="text"></td><td rowspan="2"><button type="button" class="btn-del del-source" onclick="del_source(this)">-</button></td></tr><tr><td>Source url:</td><td><input class="sources-url" value="" type="text"></td></tr></table></div>';
		$(".sources-container").append(source);
		update_form_source_names();
	});

	$('#feed_select').change(function (event) {
		var feed_index = $('select option:selected').val();
		window.location.href = '/feed/' + feed_index;
	});
});

function del_source(source) {
	$(source).closest(".source_wrap").remove();
	update_form_source_names();
}

