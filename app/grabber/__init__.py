import re
import feedparser
from app.model import *
from peewee import fn
import redis
import time
from datetime import datetime
from dateutil import parser

r = redis.StrictRedis(host='localhost', port=6379)
img_url_regex = re.compile('(https?:\/\/[^\s]*\.(?:png|jpg|gif))')

recent_img_key = 'sources:{0}:recent_img'
urls_key = 'feed:{0}:urls'
jobs_key = 'grab_jobs'

def grab_worker():
	while True:
		result = r.brpop([jobs_key], 1000)
		if result:
			source_id = result[1].decode('utf-8')
		else:
			continue
		print(source_id)
		source = (Source
				.select(Source, FeedSource)
				.join(FeedSource)
				.where(Source.id == source_id))
		try:
			source = source.get()
			process_source(source)
		except Exception as e:
			print(e)
			pass

def add_grab_job(source_ids=[]):
	print('Want to add jobs {0} to queue'.format(source_ids))
	if source_ids:
		r.lpush(jobs_key, *source_ids)
		print('Added')

def full_grab():
	fs_count = fn.Count(FeedSource.id)
	query = (Source
			.select(Source, FeedSource, fs_count.alias('count'))
			.join(FeedSource)
			.group_by(Source)
			.order_by(-fs_count))

	source_ids = [source.id for source in query]
	add_grab_job(source_ids)

def process_source(source):
	print('*' * 20)
	print('Processing source {0}'.format(source.id))
	new_images_count = 0
	try:
		feed = feedparser.parse(source.url)
	except:
		return

	feeds = (Feed
			.select(Feed, FeedSource)
			.join(FeedSource)
			.where(FeedSource.source == source))

	recent_img = r.get(recent_img_key.format(source.id))
	if recent_img:
		recent_img = recent_img.decode('utf-8')

	p = r.pipeline()

	new_recent_img = None
	if feed['items']:
		first_img_urls = img_url_regex.findall(str(feed['items'][0]))
		if first_img_urls:
			new_recent_img = first_img_urls[0]

	found_recent = False
	for item in feed['items']:
		if found_recent:
			break
		img_urls = img_url_regex.findall(str(item))
		for img_url in img_urls:
		#	if img_url == recent_img:
		#		found_recent = True
		#		break
			if not 'published' in item:
				continue
			dt1 = parser.parse(item['published']).replace(tzinfo=None)
			dt2 = datetime(1970, 1, 1)
			zindex = (dt1 - dt2).total_seconds()
			new_images_count += 1
			for feed in feeds:
				p.zadd(urls_key.format(feed.id), zindex, img_url)

	#if new_recent_img != None and new_recent_img != recent_img:
	#	p.set(recent_img_key.format(source.id), new_recent_img)
	print('Added {0} images'.format(new_images_count))
	print('*' * 20, '\n')
	p.execute()
