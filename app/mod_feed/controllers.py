from flask import Blueprint, request, render_template, flash, redirect, url_for
from flask_login import current_user, login_required
from .forms import FeedDataForm, SourceDataForm, DeleteFeedForm
from app import app, db
from app.model import *
from app.grabber import add_grab_job
import redis

mod_feed = Blueprint('feed', __name__, template_folder='templates')

@mod_feed.route('/feed/')
@mod_feed.route('/feed/<int:feed_index>')
@login_required
def view_feed(feed_index=0):
	try:
		page = int(request.args.get('page'))
		if not page or page < 0:
			raise Exception("Bad page parameter")
	except:
		page = 0

	user = current_user._get_current_object()
	feeds = (Feed
			.select(Feed)
			.where(Feed.user == user))

	if feed_index + 1 > len(feeds):
		img_urls = []
		flash('No feed with this index exists')
	else:
		img_urls = get_image_urls(feeds[feed_index].id, page)
	return render_template(
			'feed.html',
			img_urls=img_urls,
			feeds=feeds,
			feed_index=feed_index,
			page=page)

@mod_feed.route('/feed/manage/')
@login_required
def manage_feeds():
	user = current_user._get_current_object()
	del_form = DeleteFeedForm()
	feeds = (Feed
			.select(Feed)
			.where(Feed.user == user))
	return render_template('manage_feeds.html', feeds=feeds, del_form=del_form)

@mod_feed.route('/feed/manage/add/', methods=['GET', 'POST'])
@login_required
def add_feed():
	user = current_user._get_current_object()
	form = FeedDataForm(request.form)
	if request.method == 'POST' and form.validate():
		sources_to_update = []
		with db.atomic():
			feed = Feed.create(name=form['name'].data, user=user)
			for source_data in form.sources.entries:
				source, _ = Source.get_or_create(url=source_data['url'].data)
				sources_to_update.append(source.id)
				source_feed = FeedSource.create(
						name=source_data['name'].data,
						feed=feed,
						source=source)
		if sources_to_update:
			add_grab_job(sources_to_update)

		flash('Added new feed')
		return redirect(url_for('feed.manage_feeds'))
	else:
		return render_template('add_feed.html', form=form)

@mod_feed.route('/feed/manage/edit/<int:feed_id>', methods=['GET', 'POST'])
@login_required
def edit_feed(feed_id=0):
	user = current_user._get_current_object()
	feed = (Feed
			.select(Feed, FeedSource, Source)
			.join(FeedSource)
			.join(Source)
			.where(Feed.user == user, Feed.id == feed_id))

	if len(feed) == 0:
		flash('Feed with this index does not exist')
		return redirect('feed.manage_feeds')
	else:
		feed = feed.get()

	form = FeedDataForm(request.form)

	if request.method == 'POST':
		if form.validate():
			sources_to_update = []
			with db.atomic():
				feed.name = form['name'].data
				feed.save()
				(FeedSource
						.delete()
						.where(FeedSource.feed == feed)).execute()
				for source_data in form.sources.entries:
					source, _ = Source.get_or_create(url=source_data['url'].data)
					sources_to_update.append(source.id)
					source_feed = FeedSource.create(
							name=source_data['name'].data,
							feed=feed,
							source=source)
			if sources_to_update:
				add_grab_job(sources_to_update)
			flash('Changes saved')
	else:
		form = FeedDataForm()
		form.name.data = feed.name
		for feed_source in feed.feed_sources:
			source_form = SourceDataForm()
			source_form.name = feed_source.name
			source_form.url = feed_source.source.url
			form.sources.append_entry(source_form)

	return render_template('edit_feed.html', form=form, feed_id=feed_id)

@mod_feed.route('/feed/manage/delete/', methods=['GET', 'POST'])
@login_required
def delete_feed():
	user = current_user._get_current_object()
	if request.method == 'POST':
		form = DeleteFeedForm(request.form)
		if form.validate():
			feed = Feed.get(id=form.feed_id.data)
			#FeedSource.delete().where(FeedSource.feed == feed)
			feed.delete_instance(recursive=True)
			flash('Succsessfully deleted feed')
		else:
			flash('Error occured while deleting feed')

	return redirect(url_for('feed.manage_feeds'))


def get_image_urls(feed_id, page=0):

	urls_key = "feed:{0}:urls".format(feed_id)

	r = redis.StrictRedis(host='localhost', port=6379)

	urls = r.zrange(urls_key, -20 - 20 * page, -1 - 20 * page)
	urls.reverse()


	return [url.decode('utf-8') for url in urls]
