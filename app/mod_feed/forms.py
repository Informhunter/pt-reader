from flask_wtf import FlaskForm
from wtforms.widgets import HiddenInput
from wtforms import FieldList, FormField, TextField, IntegerField, validators, Form

class SourceDataForm(Form):
	name = TextField('Source name',
			[validators.required(), validators.Length(min=1, max=100)])

	url = TextField('Source URL',
			[validators.required(), validators.URL()])

class FeedDataForm(FlaskForm):
	name = TextField('Feed name',
			[validators.required(), validators.Length(min=1, max=100)])

	sources = FieldList(FormField(SourceDataForm), max_entries=10)

	def __str__(self):
		return "{0} {1}".format(self.name.data, str(self.sources))

class DeleteFeedForm(FlaskForm):
	feed_id = IntegerField('Feed id',
			[validators.required(), validators.NumberRange(min=0)],
			widget=HiddenInput())
