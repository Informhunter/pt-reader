from flask import Flask
from peewee import SqliteDatabase
from flask_login import LoginManager

app = Flask(__name__)
app.config.from_object('config')

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'auth.login'

db = SqliteDatabase(app.config['DB_NAME'])

from app.mod_auth.controllers import construct_mod_auth
from app.mod_feed.controllers import mod_feed as feed_module
#from app.mod_manage.controllers import mod_manage as manage_module
#from app.mod_settings.controllers import mod_settings as settings_module

app.register_blueprint(construct_mod_auth(starting_page='feed.view_feed'))
app.register_blueprint(feed_module)
#app.register_blueprint(manage_module)
#app.register_blueprint(settings_module)
