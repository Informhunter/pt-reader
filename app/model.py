from app import db
from peewee import *
import datetime

class BaseModel(Model):
	class Meta:
		database = db

class User(BaseModel):
	vk_id = IntegerField(unique=True)
	vk_access_token = CharField(max_length=255)
	first_name = CharField(max_length=255, null=False)
	last_name = CharField(max_length=255, null=False)
	#small_img_url = CharField(max_length=2000, null=False)

	def is_authenticated(self):
		return True

	def is_active(self):
		return True

	def is_anonymous(self):
		return False

	def get_id(self):
		return self.vk_id

class Feed(BaseModel):
	name = CharField(max_length=100, null=False)
	user = ForeignKeyField(User, related_name='feeds')

	def __str__(self):
		return "{0}".format(self.name)

class Source(BaseModel):
	url = CharField(max_length=2100, null=False, unique=True)

class FeedSource(BaseModel):
	name = CharField(max_length=100)
	feed = ForeignKeyField(Feed, related_name='feed_sources')
	source = ForeignKeyField(Source)

def create_tables():
	db.connect()
	db.create_tables([User, Feed, Source, FeedSource])

